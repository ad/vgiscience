# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "vgiscience-presentation-theme"
  spec.version       = "0.6.5"
  spec.authors       = ["Marc Löchner"]
  spec.email         = ["marc.loechner@vgiscience.org"]

  spec.summary       = "A jekyll theme for VGIscience-styled HTML presentation slides"
  spec.homepage      = "https://gitlab.vgiscience.de/design/themes/presentations/vgiscience"
  spec.license       = "BSD-2-Clause"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 3.7"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12"
end
